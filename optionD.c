#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param argv
 * @return
 */
int total(char **argv) {
  struct dirent *read;
  struct stat ptr;
  DIR *target = opendir(".");
  int i = 0;
  int stock;
  int result;

  if (target == NULL)
    return (1);
  while ((read = readdir(target)) != NULL) {
    if (read->d_name[0] != '.') {
      lstat(read->d_name, &ptr);
      stock += ptr.st_blocks;
    }
  }
  myPutStr("total ");
  myPutNbr(stock / 2);
  myPutChar('\n');
}

/**
 *
 * @param argv
 * @param check
 * @return
 */
char *pathFlag(char **argv, int check) {
  if (argv[check + 1]) {
    return (argv[check + 1]);
  }
  return (".");
}

/**
 *
 * @param argv
 * @param fil
 * @return
 */
int	startD(char **argv, int fil) {
  myPutStr(pathFlag(argv, fil));
  myPutChar('\n');
  return (0);
}
