#include <zconf.h>

/**
 *
 * @param c
 */
void myPutChar(char c){
    write(1, &c, 1);
}

/**
 *
 * @param str
 */
void myPutStr(char *str){
    int i = 0;

    while (str[i]){
        myPutChar(str[i++]);
    }
}

/**
 *
 * @param str
 * @return
 */
int myStrLength(char *str){
    int i = 0;

    while (str[i]){
        i += 1;
    }
    return i;
}

/**
 *
 * @param nbr
 * @return
 */
int	myPutNbr(int nbr) {
    int	div = 1;

    if (nbr < 0) {
        myPutChar('-');
        nbr = nbr * (-1);
    }
    while ((nbr / div) >= 10) {
        div = div * 10;
    }
    while (div > 0) {
        myPutChar(nbr / div + 48);
        nbr = nbr % div;
        div = div / 10;
    }
    return (nbr);
}