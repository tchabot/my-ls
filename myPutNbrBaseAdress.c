#include "include/my_ls.h"
#include "include/my.h"

 /**
 *
 * @param nbr
 * @param base
 * @param z
 * @return
 */
int myPutNbrBaseAdress(long nbr, char *base, int z)
{
  long   div = 1;
  long   size = myStrLength(base);

  myPutChar('0');
  z++;
  myPutChar('x');
  z++;
  if (nbr < 0) {
    myPutChar('-');
    z++;
    nbr = nbr *(-1);
  }
  while ((nbr / div) >= size) {
    div = div * size;
  }
  while (div > 0) {
      myPutChar(base[nbr / div]);
      z++;
      nbr = nbr % div;
      div = div / size;
  }
  return (z);
}
