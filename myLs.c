#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param argv
 * @param fil
 * @param i
 * @return
 */
int startLs(char **argv, int fil, int i)
{
  struct dirent *read;
  DIR *target;
  char **tab;
  char *source;

  source = path(argv, fil);
  target = opendir(source);
  if (target == NULL){
    exit(1);
  }
  if ((tab = malloc(sizeof(char*) * 5000)) == NULL){
    exit(1);
  }
  while ((read = readdir(target)) != NULL) {
    if (read->d_name[0] != '.') {
      if ((tab[i] = malloc(sizeof(char) * 256 )) == NULL) {
        return (1);
      }
      tab[i] = read->d_name;
	  i++;
    }
  }
  showTabLs(tab);
  closedir(target);
  return (0);
}
