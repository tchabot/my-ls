#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param tab
 * @param count
 * @return
 */
int showTabR(char **tab, int count) {
  while (count > 0) {
    myPutStr(tab[count - 1]);
    myPutChar('\n');
    count--;
  }
  return (0);
}

/**
 *
 * @param argv
 * @param fil
 * @param count
 * @param i
 * @return
 */
int startR(char **argv, int fil, int count, int i) {
  struct dirent *read;
  char **tab;
  char *source = pathFlag(argv, fil);
  DIR *target = opendir(source);

  if (target == NULL)
    return (1);
  if ((tab = malloc(sizeof(char*) * 5000)) == NULL) {
    return (1);
  }
  while ((read = readdir(target)) != NULL) {
    if (read->d_name[0] != '.') {
      if ((tab[i] = malloc(sizeof(char) * 256 )) == NULL) {
        return (1);
      }
      tab[i++] = read->d_name;
	  count++;
    }
  }
  showTabR(tab, count);
  closedir(target);
  return (0);
}
