#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param nbr
 * @return
 */
unsigned int myPutNbrUnsigned(unsigned int nbr) {
  unsigned int	div = 1;

  if (nbr < 0) {
      myPutChar('-');
      nbr = nbr * (-1);
  }
  while ((nbr / div) >= 10) {
    div = div * 10;
  }
  while (div > 0) {
      myPutChar(nbr / div + 48);
      nbr = nbr % div;
      div = div / 10;
  }
  return (nbr);
}
