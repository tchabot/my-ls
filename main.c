#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param tab
 * @return
 */
int showTabLs(char **tab) {
    int i = 0;

    while (tab[i]) {
        myPutStr(tab[i++]);
        myPutChar('\n');
    }
    return (0);
}

/**
 *
 * @param argv
 * @return
 */
int flag(char **argv) {
    int i = 1;
    int j = 0;

    while (argv[i]) {
        while (argv[i][j]) {
            if (argv[i][j] == '-'){
                return (1);
            }
            j++;
        }
        j = 0;
        i++;
    }
    return (0);
}

/**
 *
 * @param argv
 * @param check
 * @return
 */
char *path(char **argv, int check) {
    if (argv[check])
        return (argv[check]);
    return (".");
}

/**
 *
 * @param argv
 * @param i
 * @param j
 */
void checkFlag(char **argv, int i, int j) {
    if (argv[i][j + 1] == 'l')
        startL(argv, i, 0);
    if (argv[i][j + 1] == 'r')
        startR(argv, i, 0, 0);
    if (argv[i][j + 1] == 'd')
        startD(argv, i);
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    int i = 1;
    int j = 0;
    int res = flag(argv);

    while ((argv[i] && argv[i - 1]) || i == 1) {
        if (res == 1) {
            while (argv[i][j] != '-') {
                j++;
            }
            checkFlag(argv, i, j);
            i++;
        }
        if (res == 0) {
            startLs(argv, i, 0);
        }
        i++;
    }
    return (0);
}
