#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include "include/my_ls.h"
#include "include/my.h"

/**
 *
 * @param ptr
 */
void type(struct stat ptr)
{
  if (S_ISDIR(ptr.st_mode))
    myPutChar('d');
  if (S_ISLNK(ptr.st_mode))
    myPutChar('l');
  if (S_ISREG(ptr.st_mode))
    myPutChar('-');
  if (S_ISSOCK(ptr.st_mode))
    myPutChar('s');
  if (S_ISBLK(ptr.st_mode))
    myPutChar('b');
  if (S_ISFIFO(ptr.st_mode))
    myPutChar('p');
  if (S_ISCHR(ptr.st_mode))
    myPutChar('c');
}

/**
 *
 * @param ptr
 */
void rights(struct stat ptr) {
  (ptr.st_mode & S_IRUSR) ? myPutChar('r') : myPutChar('-');
  (ptr.st_mode & S_IWUSR) ? myPutChar('w') : myPutChar('-');
  (ptr.st_mode & S_IXUSR) ? myPutChar('x') : myPutChar('-');
  (ptr.st_mode & S_IRGRP) ? myPutChar('r') : myPutChar('-');
  (ptr.st_mode & S_IWGRP) ? myPutChar('w') : myPutChar('-');
  (ptr.st_mode & S_IXGRP) ? myPutChar('x') : myPutChar('-');
  (ptr.st_mode & S_IROTH) ? myPutChar('r') : myPutChar('-');
  (ptr.st_mode & S_IWOTH) ? myPutChar('w') : myPutChar('-');
  (ptr.st_mode & S_IXOTH) ? myPutChar('x') : myPutChar('-');
}

/**
 *
 * @param ptr
 * @return
 */
int	name(struct stat ptr)
{
  struct passwd	*user_name = getpwuid(ptr.st_uid);
  struct group	*group_name;

  if (user_name == NULL)
    return (0);
  myPutStr(user_name->pw_name);
  myPutChar(' ');
  group_name = getgrgid(ptr.st_gid);
  if (group_name == NULL)
    return (0);
  myPutStr(group_name->gr_name);
  myPutChar(' ');
}

/**
 *
 * @param tab
 * @param ptr
 * @return
 */
int	infos(char *tab, struct stat ptr) {
  char *date = malloc(sizeof(char) * 13);
  int i = 4;

  if (date == NULL)
    return (0);
  date = ctime(&ptr.st_mtime);
  type(ptr);
  rights(ptr);
  myPutChar(' ');
  myPutNbr(ptr.st_nlink);
  myPutChar(' ');
  name(ptr);
  myPutNbr(ptr.st_size);
  myPutChar(' ');
  while (i < 16)
    myPutChar(date[i++]);
  myPutChar(' ');
  myPutStr(tab);
  myPutChar('\n');
  return (0);
}

/**
 *
 * @param argv
 * @param fil
 * @param i
 * @return
 */
int	startL(char **argv, int fil, int i)
{
  struct dirent	*read;
  struct stat ptr;
  DIR *target;
  char **tab;
  char *source;

  total(argv);
  source = pathFlag(argv, fil);
  target = opendir(source);
  if (target == NULL)
    return (1);
  if ((tab = malloc(sizeof(char*) * 5000)) == NULL)
    return (1);
  while ((read = readdir(target)) != NULL)
    if (read->d_name[0] != '.')
      {
	if ((tab[i] = malloc(sizeof(char) * 256 )) == NULL)
	  return (1);
	lstat(read->d_name, &ptr);
	tab[i++] = read->d_name;
	infos(read->d_name, ptr);
      }
  closedir(target);
  return (0);
}
