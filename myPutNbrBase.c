#include "include/my_ls.h"
#include "include/my.h"

 /**
 *
 * @param nbr
 * @param base
 * @param z
 * @return
 */
int	myPutNbrBase(int nbr, char *base, int z)
{
  int div = 1;
  int size = myStrLength(base);

  if (nbr < 0) {
      myPutChar('-');
      z++;
      nbr = nbr *(-1);
  }
  while ((nbr / div) >= size) {
      div = div * size;
  }
  while (div > 0) {
    myPutChar(base[nbr / div]);
    z++;
    nbr = nbr % div;
    div = div / size;
  }
  return (z);
}
