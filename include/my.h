/*
** my.h for my.h in /home/chabot_t/rendu/Piscine_C_J09/ex_02
** 
** Made by Thomas CHABOT
** Login   <chabot_t@epitech.net>
** 
** Started on  Fri Oct  9 09:48:21 2015 Thomas CHABOT
** Last update Sun Nov 29 17:15:34 2015 Thomas CHABOT
*/

void	myPutChar(char c);
int	my_isneg(int nb);
int	myPutNbr(int nb);
int	mySwap(int *a, int *b);
char myPutStr(char *str);
int	myStrLength(char *str);
int	myGetnbr(char *str);
void mySortIntTab(int *tab, int size);
int	myPowerRec(int nb, int power);
int	mySquareRoot(int nb);
int	myIsPrime(int nombre);
int	myFindPrimeSup(int nb);
char *myStrcpy(char *dest, char *src);
char *myStrncpy(char *dest, char *src, int nb);
char *myRevstr(char *str);
char *myStrstr(char *str, char *to_find);
int	myStrCmp(char *s1, char *s2);
int	myStrncmp(char *s1, char *s2, int nb);
char *myStrUpcase(char *str);
char *myStrLowcase(char *str);
char *myStrCapitalize(char *str);
int	myStrIsAlpha(char *str);
int	myStrIsNum(char *str);
int	myStrIsLower(char *str);
int	myStrIsUpper(char *str);
int	myStrIsPrintable(char *str);
int	myShowStr(char *str);
int	myShowMem(char *str, int size);
char *myStrCat(char *dest, char *src);
char *myStrnCat(char *dest, char *src, int nb);
int	myStrlCat(char *dest, char *src, int size);
